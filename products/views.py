from django.shortcuts import render
from .models import Product
# Create your views here.
def home(request):
    """
    Home page view
    """
    return render(request, 'products/home.html')

def products(request):
    """
    Products page view
    """
    products = Product.objects.all()
    context = {'products': products}
    return render(request, 'products/products.html', context)
