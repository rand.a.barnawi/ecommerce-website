from django.db import models

class Seller(models.Model):
    name = models.CharField(max_length=200)
    phone = models.IntegerField(max_length=20) #CharField
    email = models.CharField(max_length=50)
    profile = models.CharField(blank=True, max_length=200) #text field
    photo = models.ImageField(upload_to="seller/photos/")

    def __str__(self): #convert into string
        return self.name
        



class Product(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    price = models.IntegerField()
    seller = models.ForeignKey(Seller, on_delete=models.DO_NOTHING)
    photo = models.ImageField(upload_to="product/photos/")

    def __str__(self): #convert into string
        return self.name
        
